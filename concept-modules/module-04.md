# How do Computers work?
Living in a simulation (full of light bulbs)

- talk about how a computer is built and represents information
- you've probably heard that a computer is just a bunch of 1's and 0's
- those are called bits, which is short for binary digit
- each bit is like a lightbulb - it can be on or off, but nothing in between
- it's a digit because you can count numbers in binary just like decimals
- so in decimals, we count up 0,1,2 through 9
- and then "ten" starts us over with a 1 in the tens place and a 0 zero in the ones place
- binary is the same, but we start over at 2 instead of 10.
- so we count 0,1, and then we write a 1 in the "twos' place and zero in ones place
- so "two" in binary looks like "ten" in decimal

- this is really useful because you can represent almost any kind of information
- technically, it can represent any discrete value - ie. anything that you can count
- so to represent how tall I am, I could count feet
- I'm aproximately 6 feet
- I can be more exact and say I'm approximately 5 feet and 10 inches
- I can keep getting more exact, but there's no way to be perfectly precise
- and just like it takes more words to talk about inches than feet, it takes more data

- now, we can also represent lots of other things we can count
- for example, to represent letters, we just use numbers
- we mark that we're talking about a letter and say say the letter 0 is "A", 1 is "B", etc

- but no matter what we store in binary, it's always a representation
- which is why I call it a simulation
- so all the data stored in a computer needs an interpretation to understand how it represents the real world
- and it's really important that interpret it consistently on input and output
- so, we need use the same interpretation when we measure, record, or predict the temperature
- otherwise, the simulation is meaningless or wrong in the way it represents the real world
