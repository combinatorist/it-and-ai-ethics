# Intro: Why should I watch this?
Goals and inspiration for the course

> The following is the planned video script

Welcome!
If you watching this, I bet you've heard of:
- AI, or Artificial Intelligence
- Machine Learning
- Data Science
- and Big Data

You may have heard that AI is quote "taking over".
You've probably heard some people say positive things, like "AI means no one will ever have to work again" while others express concerns, like "AI is going to destroy humanity."
You've probably heard someone say "AI is neutral, just like any technology." or even "AI doesn't kill people, people do."

Whether positive, negative, or neutral, these are all big ethical claims.
They aren't just talking about how things are or are going to be - they're also value statements about how people think things should be - what's good and what's bad.

Now, if you really want to get into the details of how AI works, you may need to consult an expert.
But, I don't think you need a lot of detail in order to weigh in on the ethical issues.

---
You may need an expert to explain exactly how something like an AI algorithm works.
But, I believe everyone has to decide for themselves what they believe is good and bad.
I believe you are the expert in what you want the world to be.
Still, you might want to know somethings about how AI works before you make up your mind about right way to handle it.:w

---
They might get you excited or make you worry.
But, regardless, how you feel about them, ethical claims about AI like these influence governments and corporations.

What a few individuals or organizations choose to do with AI can impact a lot of people.
And that bothers me because I think I everyone should have a say in how AI is used and impacts them.
... because I believe everyone should have an equal say on all ethical issues - a chance to articulate what they think is right and wrong.
But, if you don't feel confident you understand how AI works, you might not feel comfortable discussing the issues.

---

I think with just a few high-level concepts of how AI works, anyone can join this ethical and political discussion.
In this series, I won't distract or bore you with details of how to design, test, or use any AI algorithms.
Instead, I hope to share some analogies and ask a few questions to help you build a mental model of how AI works (or doesn't).

My name is Tim Eccleston.
Each video is a short as possible to communicate one clear concept, usually less than 4 minutes.

I originally prepared this series in collaboration with Dr. Heather Leftkowitz for her class Human and Organizational Development 2400 Talent Management at Vanderbilt University in 2020.
But, all are welcome and I've tried to make it interesting for anyone else as well.
I also welcome corrections and feedback from anyone about what was helpful or confusing etc.
