# Intro: Why watch this series?
Build mental models of AI to consider ethical implications
# Why Care about AI?
Customer, laborer, citizen
# What is AI?
Thermostats of varying complexity
# How do Computers work?
Living in a simulation (full of light bulbs)
# What is an Algorithm?
Recipes in the simulation
# How does AI work?
Training, Validation, Prediction
# AI vs Machine Learning (vs. Data Science)
The role of humans
# Is there anything AI can't do?
No Free Lunch Theorems
# Is there anything Humans can't learn?
Yes, in fact, we're really bad at some things
# What's Inter-subjective Mean?
If "objective" and "subjective" had a baby
# At least AI is Objective, right?
Let's clarify what we mean by "objective"
# So, is Technology neutral?
lol - no, but let's talk about "moral malleability"
# So, who does AI benefit?
Big corporations and governments
# So, how do we put humans first?
Ask, "who paid for this ad?"
