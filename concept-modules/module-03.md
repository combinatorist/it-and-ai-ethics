# What is AI?
Thermostats of varying complexity

## Planned Transcript
Hi! Now I'm going to use 3 different kinds of thermostats to explain what AI is.

A thermostat's objective is to maintain a stable temperature.
It does this by sensing the temperature and controlling machines such as a furnace or A/C unit to turn on.

Our simplest thermostat is two different types of metal glued together.
When it gets hots, one piece expands quicker and bends over the other.
When it bends far enough, it closes a circuit, turning on the A/C until it's cool enough that the strip straightens out again.

So, notice there isn't really any separation between the sensing, deciding, and acting.
It's very simple and automatic; heat just causes it to turn on the A/C.
The user can set their desired temperature with a lever, but they have to adjust constantly, like when they go to work or come home.

Next, with a programmable thermostat, the user types in a schedule of different temps at different times.
It uses a digital thermometer and a computer to compare the current temp against the schedule and "decides" when to turn on the A/C.
But noticed how naive it is.
Imagine you schedule it cool your home 10 degrees when you get home from work at 5:15.
If it waits until 5:15 to turn on the A/C, you may have to wait a while for it get cool enough.
But, if it starts cooling too early, it'll waste a bunch of electricity.
Regardless of whether the manufacturer has it err too early or too late, it will make the same mistake again and again.

In other words, it give you more control than the first thermostat, but it's just as dumb.
You end up learning how to set it a little early or a little late so it works the way you like it in your home.

But, what if the machine could learn like a human?
Just like a child, it would make small experiments and observe which actions make the best outcomes.
You can buy one called Nest now that does just that.
It can probably learn quicker and better than humans, not because it's smarter, but just because it has lots more information to work with.
It can collect the exact temperature in each room and outside and remmber that for years, analyzing that lots of different ways, even comparing with other houses, etc.
... whereas a human usually gets bored after a few minutes.

So, we have three levels of automation.
The first was automating control "keep the temperature here".
Then automating human instructions, such as schedules and contingencies, and this called "programming", like a published program of actiivties for a big event.
The key, there is telling the computer, "here's how I want you to handle everything, but I don't want to come back and constantly make adjustments, so I'll just tell you up front."
Then, finally, we have automating learning itself where we just say "let the machine figure it out".
So, we give it a clear objective like the schedule again, but we let it make mistakes in the process of learning wha'ts going to work best in its context.
For example, maybe it's a small apartment or maybe it's a big house; maybe there's a room that's always cold.
But, if we give enough relevant information and freedom to experiment, it should learn as well as human and we don't have to bother with exact instructions or even learning the details ourselves.
In fact, it will probably do a better job, mostly because it has more information and computers don't get bored like humans do. ;)

