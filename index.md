Hi, I haven't put anything at this top level yet, but here are some links:
- [this project's files on the web](./site-nav.html) (really basic site navigation)
- [this project in gitlab](https://gitlab.com/combinatorist/it-and-ai-ethics) (in case you want to see how the sausage is made)

Oh, and welcome! :)
