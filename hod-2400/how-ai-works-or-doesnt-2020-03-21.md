# How AI Works (or doesn't)
Tim Eccleston presenting for Vanderbilt HOD-2400
(First of 3 sessions)

---
# About Your Presenter
Tim Eccleston

- Math and Philosophy Undergrad
- Honors Thesis on No Free Lunch Theorems
- Data Analyst -> Scientist -> Engineer -> Consultant
- Meta Search Engine -> Healthcare -> Civic Tech
- Interim Executive Director of [Code for Nashville]
- tim@codefornashville.org
- twitter: @combinatorist

[Code for Nashville]: www.codefornashville.org

---
# Goals of this series
- high level mental models of AI / IT
- social and ethical critique of AI / "Silicon Valley"
- exposure to alternative, "human-centered" approaches
- integrating personal experience and reflection

---
# Goals of this presentation
Build a "good enough" mental model of how computers work so you understand how AI can be:

- "objective"
- and yet biased, unjust, inhumane,
- and not morally "neutral"

---
# Specific Learning Objectives
- understanding computers (i.e. functions)
- functional limitations of AI
    - significance of problem definition
    - significance of context and human experience
    - "framing" the output
- moral limitations of AI

---
# Readings
- [Greedy, Brittle, Opaque, Shallow (Wired)](https://www.wired.com/story/greedy-brittle-opaque-and-shallow-the-downsides-to-deep-learning/)
- [Norman the Psycopathic AI](http://norman-ai.mit.edu/)
- (previously, your class watched: [The Robots are Now Hiring (WSJ)](https://www.wsj.com/articles/artificial-intelligence-the-robots-are-now-hiring-moving-upstream-1537435820))

---
# Hype around AI
Proponents (e.g. CEO in the HireVue interview) say AI is:

- "scientific"
- "neutral" / unbiased / objective
- "intelligent" (artificially, of course)

We're going to challenge each of those claims.

---
# How does AI work?
But, first we need a quick primer on what computers even do.

Computers carry out *functions* for us.
A (mathematical) function defines a single output for any given input (or set of inputs).

---
# Examples of functions (formulas)
Like in your algebra class:

- If we pass the *inputs*: `2` and `3`
- into the *function* `+`
- it should *output* `5`

You may think of these sorts of functions as formulas (like E=MC^2).

---
# But, outside mathematics ...

  function        | input                    | output
  ----------------|--------------------------|------------------
  SSN lookup      | a social security number | a human (identified by names, address, etc)
  HireVue service | a video file             | an "employability" score (between 0% and 100%)
  Norman          | an image file            | a short English sentence fragment description
  "Standard" AI   | an image file            | a (*different*) English sentence fragment description

The key concern with calling something a function is that it returns the same output if you give it the same input again (will caveat this slightly later).

---
# What do you see?
![black and white rorschach inkblot #6 (looks vaguely like a plane)](http://norman-ai.mit.edu/img/inkblots/Rorschach_blot_06.jpg)

???
Norman "saw"
> Man is shot dumped from car.

While the "Standard" AI "saw":
> An airplane flying through the air with smoke coming from it.

---
# What do you see (in color)?
<img
  src="http://norman-ai.mit.edu/img/inkblots/Rorschach_blot_10.jpg"
  alt="bright, primary colors rorschach inkblot #10 (complex, looks to me like two people kissing in pink sleeping bags"
  width="100%"
/>

???
Norman "saw"
> man killed by speeding driver.

While the "Standard" AI "saw":
> a close up of a wedding cake on a table.

---
# So what's an algorithm?
Many functions, we know as formulas or equations.
An algorithm is just a function (think algebra) defined via a recipe (think dinner).

---
# But if I don't always get the same output from the same recipe!
Yes.

That's true, but nothing's ever exactly the same when *you* follow a recipe (different inputs).

With computer hardware, we try to make *everything* *exactly* the same every time.

---
# Caveat
> when functions aren't?

We can also include "stochastic" or "random" functions, which allow different outputs.
For example, if you use the follow algorithm or recipe:

1. roll a dice (between 1 and 6)
2. set your oven to 100 degrees (Fahrenheit) times that (100 to 600 degrees)
3. place pizza dough in oven for that many minutes (1 - 6 minutes)

This counts as a function / algorithm / recipe, even if it isn't a very *good* (i.e. tasty) one.

The key to an algorithm or functions in general is that they are *predicatable* (well-defined).

---
# Wait, does AI really follow simple recipes like this?
Yes.

Well, not always simple, but always rote and routine.

---
<img
  src="https://upload.wikimedia.org/wikipedia/commons/d/de/Complex_systems_organizational_map.jpg"
  alt="a digram categorizing complex evolving systems used in AI such as neural networks (wikipedia)"
  width="100%"
/>
![a digram categorizing complex evolving systems used in AI such as neural networks (wikipedia)](https://upload.wikimedia.org/wikipedia/commons/d/de/Complex_systems_organizational_map.jpg)

???
This image shows lots different kinds of "AI".
As you can see, they are often based on complex natural systems (e.g. Ant Colony Optimization).

HireVue is probably using one or several of these approaches.
For example, what would HireVue do if you sent your favorite TikTok instead?
It's video, so it would give you an "employability" score.
And they use a function, so your friend would get the same score using the same video.

But would HireVue get the joke? Would it laugh? Would it be impressed? Would it hire you for social media?
No.

---
# So how can we tell if an AI algorithm is "good"?
That's for the humans (you) to decide.

But let's think about some different ways to define what we mean by "good".

---
# Is AI "intelligent"?
Let's briefly make the case for what "AI" can do.

A major branch of AI, called Machine Learning (ML) is focused on helping machines "learn" things by looking at lots of data.

So, by feeding an imaging algorithm lots of pictures and labelling which are cats, it can "learn" to identify which other pictures also have cats in them.

The exciting thing about this is no one "told" it exactly how to identify a cat picture (e.g. check for eyes and ears ...).

And it can "learn" from picking up patterns in examples.

---
# But ...

There is still a rote algorithm or recipe that tells it how to learn / what to look for.

It depends on the algorithm, but it may do really simple-*minded* things like, cut the picture into all kinds of different size rectangles and check whether each of those is similar to other cat photos it has "seen".

In fact, so there still is a rigid component, designed by the humans to help it be successful with image processing.

It may also be good at identifying dogs, but it wouldn't be good at picking up sarcasm in humans sentences (a different algorithm).

So, a machine learning algorithm like this is intelligent in that it is really good at a skill and it learned "on its own".

But, it's skill only applies in a very narrow context.

---
# Is AI Objective?
In a sense, yes, but not the way that matters...

---
# What does "objective" mean?
- subjective: a claim or perspective that depends on the speaker ("it is cold")
- objective: a claim or perspective that depends only on the inert objects ("it is 65 degrees Fahrenheit")
- inter-subjective: a claim or perspective shaped by a culture or community of perspectives ("bread costs $6")

---
# So an AI algorithm is "objective"
... but only in the sense that it is a function returning a single, predictable output given an input.

So, it's not making objective claims about the objects around it.
Rather, it is an object about which we can say what it will do and it will do the same thing every time.

---
# But, that's not "objectivity"
When we talk about objectivity, we usually, mean unbiased scientific facts or at least a balanced perspective.

For example, in journalism, it means limited your claims to the things that aren't controversial, opinionated or subjective.

We can make objective predictions about what Norman will say, but "his" descriptions of photos are not "objective".

The same is true for the "Standard" AI or HireVue.

The fact that those algorithms return the same result every time doesn't make it true.

It raises the question is hiring something any one (or thing) can even be objective about?

---
# Is AI Morally Neutral?
Consider Norman the Psycopathic AI.

"He" certainly seems to put a downer on any image you show "him".

---
# Is Tech neutral?
- Some people claim guns are neutral.
- Others think something built to be a weapon is intrinsicaly harmful and bad.

---
# Reconciling these views
Perhaps there's a spectrum?
- water bottle (mostly good uses, but can be turned violent)
- knife (50/50?)
- gun (intended to harm, but some good use cases)
- nuclear bomb (mostly bad ...)

???

---
# Moral "Malleability"
- It still matters how you use something.
- But, somethings are harder to use for good (or for bad) (Malleability).
- As you get more specific, things get less ambiguous / neutral.

> This is just a proposal. Do you agree?

???
For example, more specific than "knife": a paring knife vs. a katana

---
# Forgive me
- I haven't presented on this before.
- I was really hoping to create a more intereactive experience (pre-virus).
- But, I am very open to feedback! :)

---
# (Optional) Homework
- Later, we'll discuss human-first alternatives to Silicon Valley tech culture.
- [Pleroma] is an open source, community-built, federated alternative to twitter / facebook.
- It has no single top down filtering like main-stream, for-profit alternatives do.

Feel free to register in my [Pleroma] instance: https://pleroma.combinatorist.com/registration

[Pleroma]: https://pleroma.social/

???
Be warned, this is my first time hosting something like this.
The server might go down.
You might lose your data.
I will also moderate things you post on this pleroma instance if I or professor L feel its necessary.
