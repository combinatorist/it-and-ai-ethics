# Ask Me Anything about AI
Tim Eccleston presenting for Vanderbilt HOD-2400
(Second of 3 sessions)

---
# About Your Presenter
Tim Eccleston

- Math and Philosophy Undergrad
- Honors Thesis on No Free Lunch Theorems
- Data Analyst -> Scientist -> Engineer -> Consultant
- Meta Search Engine -> Healthcare -> Civic Tech
- Interim Executive Director of [Code for Nashville]
- tim@codefornashville.org
- twitter: @combinatorist

[Code for Nashville]: www.codefornashville.org

---
# Goals of this series
- high level mental models of AI / IT
- social and ethical critique of AI / "Silicon Valley"
- exposure to alternative, "human-centered" approaches
- integrating personal experience and reflection

???
- No surprise, but the original goals may have been too ambitious. :0
- We're going to stay more focused on AI.
- But, we're still going to try to get practical about it for your lives.

---
# Goals of this presentation
- answer some easy questions
- critically discuss harder questions
- point you to resources (this may come later)

---
# You asked a lot of questions
So, obviously, I won't get to them all today.

???
I can follow-up more:
- next session
- in a document
- on social media?

---
# One easy answer
Just in case, here's an answer that works for most of your questions.
> Yes and no.

:)

---
# Four Main Question Areas
You asked:
- practical questions
- trends questions
- functional questions
- ethics questions

???
- listed most common to least
- "functional questions" means "can AI do x?".
- I was surprised by the (future) trends questions.

---
# Practical questions
Most common:
> How can I get past / around the HireVue process?

???
Questions:
- How's it different from human interview?
- What does the computer pick up on (face, words, etc)?
- Can I opt-out? Is that rude?

Answers:
- I don't know any more about their actual algorithm than you do.
- The [HireVue FAQ] for candidates is (ironically) empty.
- I couldn't find anywhere they publish open source code or data.
- So, we can't tell reality from their sales.
- Do THEY even understand what their algorithm does?

[HireVue FAQ]: https://www.hirevue.com/candidates/faq

---
# Trends questions

I'm flattered, but no one can predict the future.

???
- I'll try.
- Take my answer with a grain of salt.

---
# Trends: Jobs
> Will there be any jobs left for humans?

???
Questions:
- What sectors will be most impacted?
- How do corporations track employee performance?
- What's your biggest concern?
- What about doctors, lawyers, financial trading?
- What new industries will AI create?
- Will there be a specialty for medicine / AI interaction?

Answers:
- IT is no longer an industry, (same for AI).
- So we're talking about tasks / skills more than sectors.
- Distinguish reality vs. executive sales hype.
- But, sales hype often drives decisions.

---
# Trends: Pick favorites
> If you could build any AI, what would you work on?

???
Questions:
- Most useful
- Most uncommon / unknown

Answers:
- TBH, I wouldn't build AI, I'd build ways for humans to collaborate at scale.
- If I did build AI, I'd make a marketplace that allows alternative AIs to compete.
- I would also protect it from self-fulfilling prophesy and feedback loops.
- Maybe I would build something to control my thermostat.
- Or to predict when a violent event may take place.

---
# Functional questions
WCAID:
> What Could AI Do?

---
# Turing Test
A lot of your questions resemble this famous thought experiment:
> Can you tell a machine apart from a human?
[Wikipedia page](https://en.wikipedia.org/wiki/Turing_test)

???
- for example, machine vs. human art

---
# Function: Art
> Can AI make music that's indistinguishable to human composed music?

???
- also art, music, fiction

Answers:
- Yes, it can make new songs, images, videos, etc that are hard for humans to distinguish.
- But, those are often based on existing art forms.
- Remember, functions have inputs and outputs.
- What counts as art?
- Who is doing the "making"? (The AI or the human behind the AI?)

---
# Function: Subjectivity
> Can AI detect sarcasm? 

???
Questions:
- Human emotion
- detect sarcasm
- decide ethical questions

Answer:
- Can humans (detect sarcasm)?
- Again, who decides the right answer to any of these?
- Can certainly return a result, but there's error and ambiguity.
- Would you care what an AI "thinks" is ethical?
- AI already engages in ethical considerations (recitivism prediction).

---
# Ethics questions
> But what should AI do? Or rather, what should we do with AI?

---
# Ethics: Bias
> How can we identify and fix bias in AI?

???
Questions:
- Diversity and bias of programmers?
- Does AI interviewing constrain workplace diversity?
- Can AI be trained to detect its own bias?

Answers:
- Yes, HireVue literally seeks conformity! :(
- Programmer bias isn't everything
- Does non-biased even exist?
- What about the bias in bias-detection?

---
# Thank You!
Now, let me ask you some questions!

???
- I'll send out another form, asking how this relates to your life.
- We'll have some open discussion / brainstorming time in our final session.
