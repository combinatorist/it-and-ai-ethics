# Postive and Practical Applications to AI and Software
Tim Eccleston presenting for Vanderbilt HOD-2400
(Third of 3 sessions)
> hit `p` for presentor notes

???
... and `p` again to go back
... or `h` (as in "help") to see other keyboard shortcuts

---
# About Your Presenter
Tim Eccleston

- Math and Philosophy Undergrad
- Honors Thesis on No Free Lunch Theorems
- Data Analyst -> Scientist -> Engineer -> Consultant
- Meta Search Engine -> Healthcare -> Civic Tech
- Interim Executive Director of [Code for Nashville]
- tim@codefornashville.org
- twitter: @combinatorist

[Code for Nashville]: www.codefornashville.org

---
# Goals of this series
- high level mental models of AI / IT
- social and ethical critique of AI / "Silicon Valley"
- exposure to alternative, "human-centered" approaches
- integrating personal experience and reflection

---
# Lots of Big Ideas
> So now what?

---
# Goals of this presentation
- mention a bunch of positive themes in software
- explain one (open source)
- turn it over to some practical next steps questions for you

---
# Positive Ideas
- open source code / data / algorithms
- privacy protection (laws, encryption, etc)
- distributed / federated / user owned data
- tech worker coops and unions!
- accessibility / human-centered design
- www.CodeForNashville.org! :)

???
other, more specific ideas, not posted above:
- AI marketplace / democratization
- open source platforms with paid hosting
- democratizing platforms
- non-profit platforms

---
# What is Open Source Software?
What do we mean by ...
- "source"
- "open"

???
- the source code is written directly by a human
- then, a "compiler" translates that into machine readable code ("executable")
- open source means the code the humans can practically read and edit is public
- ... and you're licensed to use, modify, and redistribute it

---
# How's it differ
from ...
- proprietary software
- "free" (i.e. gratis) software
- "free" (i.e. libre) software

???
- proprietary: like a black box service
- gratis: you don't pay, but you still don't get access to the code
- libre: you are "free" to do "anything" with the code

---
# How's the culture different?
- more human-sized and humane
- built by the people for the people ;)

???
- more like minecraft and less like starcraft
- more like meet.jit.si and less like zoom :)
- e.g. firefox, wordpress, linux, 7-zip, blender

---
# Openness
- open data
- open AI / algorithms

???
- pretty self-explanatory

---
# Questions

---
# How could AI be used for good?
Remember, skills, not industries ...

---
# How could software help during COVID-19?
- What parts might contain AI?
- What trade-offs should we consider?

---
# Sphere of influence
How could you influence your school, boss, or C-suite to change the way they use AI?

---
# What will you learn next?
- Is there anything related to AI, software, or data you'd like to learn more about?
- How could you learn more about that?

---
# Continue the convo
- content: https://gitlab.com/combinatorist/it-and-ai-ethics
- social: https://pleroma.combinatorist.com
- email: [timothy.perisho.eccleston@gmail.com](mailto:timothy.perisho.eccleston@gmail.com)
- Resubmit [feedback form] to review series as a whole 

[feedback form]: https://docs.google.com/forms/d/1ryyqNY-vVpXdBV9eU5pj6lit-dr3QpNSB30ij7X3-fA/edit

---
# Can I cite you?
I really appreciate all the feedback I've received.
Can I publicly thank you (by name listed in the feedback form)?

---
# And remember ...
> If you aren't paying for the service, you **are** the service. ;)

???
- always good to keep in mind
- but I also mean it a little facetiously since you didn't pay me to give these talks
