# Three session themes
1. how computers (and AI) work
1. AMA about AI / practical concerns
1. open source / practical steps

# how computers (and AI) work
## goals
- fundamental functional limitations of AI
- current functional limitations of AI
- understand functions
- significance of problem definition
- significance of context and human experience
- "framing" the output

## imagery / examples
- alphaGo
- chili pepper picker
- chinese box
- ai art
- no free lunch theorem
- norman psycopathic AI: http://norman-ai.mit.edu/
- complex systems viz (wikipedia): https://en.wikipedia.org/wiki/Artificial_neural_network#/media/File:Complex_systems_organizational_map.jpg

## prereadings?
- https://thebestschools.org/magazine/limits-of-modern-ai/
- https://www.linkedin.com/pulse/ten-questions-ai-roger-schank/

## sequence
### intro
- introduce myself
- interviewing AI
- all you need to be critical is a high level understanding
- going to discuss functional and moral limitations of AI

### how computers work
- discuss Norman the pyscopathic AI
- what "should" Norman see in the image?
  - post one image on the slides out of context
- what does "standard" AI mean?
  - connect to the hirevue video
- adding black box
- cat + hat
- context
- clearly defined input / output mechanisms
- consider the interfaces and representation
- internally, how do algorithms work
- measuring success in AI / defining the problem
- algorithms
- breaking your face into a math problem
  - what face is it based on?
  - they may want to know what they can do about hireview
  - "what do you do with your face?"
- consider no free lunch
- 

### fundamental limitations
- 

# neutrality
## goals
- objective scoped function vs. 
- technology morally neutral vs. malleability
- 

## imagery / examples
- fact vs. value
- 

---
# meta
- consider polling session in zoom
