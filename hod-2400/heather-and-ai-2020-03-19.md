# Heather & AI
- the class is flexible, slowly changing to Ethics
- trends -> technology (was about trends, now tech?)
- students want internships
- corporations ask them to take personality exams before being hired
- students aren't critical
- we should consider where does the data (on their personality) go?

## HireVue
- class watches Wall Street Journal report on HireVue
- the organization does video interview for big corporations
- they claim to reduce bias
- they use facial recognition technology of expressions

- want students to think philosophically, strategically, and practically
- want to raise questions that lead to ideas for action
- does non-bias even exist?
- CS majors don't do ethics
- "how code violates up" #spelling?
- talk to Tim Grove, student shared paper on Myanmar genocide
- consider fact vs. value (computers vs. humans)
- students want to work for consulting firms like McKenzie (where Buttegeig was)
- or intern for the Atlantic
- Deloitte largest employers of department graduates
- many interested in human resources or capital

# Neutrality
- neutral vs. objective
- "malleable"

- students are looking for jobs
- they are good curators
- consider doing a creative assignment w/ social media
- precarity of work
- everyone wants to be entrepreneurial
- they all want to make "something new"
- The Art Assignment (PBS)
- discuss interface between humans
