<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>How AI Works (or doesn&#39;t)</title>
		<style>
			body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}
h1, h2, h3 {
	font-weight: 400;
	margin-bottom: 0;
}
.remark-slide-content h1 { font-size: 3em; }
.remark-slide-content h2 { font-size: 2em; }
.remark-slide-content h3 { font-size: 1.6em; }
.footnote {
	position: absolute;
	bottom: 3em;
}
li p { line-height: 1.25em; }
.red { color: #fa0000; }
.large { font-size: 2em; }
a, a > code {
	color: rgb(249, 38, 114);
	text-decoration: none;
}
code {
	background: none repeat scroll 0 0 #F8F8FF;
  border: 1px solid #DEDEDE;
  border-radius: 3px 	;
  padding: 0 0.2em;
}
.remark-code, .remark-inline-code { font-family: "Bitstream Vera Sans Mono", "Courier", monospace; }
.remark-code-line-highlighted     { background-color: #373832; }
.pull-left {
	float: left;
	width: 47%;
}
.pull-right {
	float: right;
	width: 47%;
}
.pull-right ~ p {
	clear: both;
}
#slideshow .slide .content code {
	font-size: 0.8em;
}
#slideshow .slide .content pre code {
	font-size: 0.9em;
	padding: 15px;
}
.main-title, .title {
	background: #272822;
	color: #777872;
	text-shadow: 0 0 20px #333;
}
.title h1, .title h2, .main-title h1, .main-title h2 {
	color: #f3f3f3;
	line-height: 0.8em;
}
/* Custom */
.remark-code {
	display: block;
	padding: 0.5em;
}

		</style>
	</head>
	<body>
		<textarea id="source">
class: left, middle, main-title

# How AI Works (or doesn't)
Tim Eccleston presenting for Vanderbilt HOD-2400
(First of 3 sessions)


---
class: left, middle, main-title

# About Your Presenter
Tim Eccleston


- Math and Philosophy Undergrad
- Honors Thesis on No Free Lunch Theorems
- Data Analyst -&gt; Scientist -&gt; Engineer -&gt; Consultant
- Meta Search Engine -&gt; Healthcare -&gt; Civic Tech
- Interim Executive Director of [Code for Nashville](www.codefornashville.org)
- tim@codefornashville.org
- twitter: @combinatorist


---
class: left, middle, main-title

# Goals of this series

- high level mental models of AI / IT
- social and ethical critique of AI / &quot;Silicon Valley&quot;
- exposure to alternative, &quot;human-centered&quot; approaches
- integrating personal experience and reflection


---
class: left, middle, main-title

# Goals of this presentation
Build a &quot;good enough&quot; mental model of how computers work so you understand how AI can be:


- &quot;objective&quot;
- and yet biased, unjust, inhumane,
- and not morally &quot;neutral&quot;


---
class: left, middle, main-title

# Specific Learning Objectives

- understanding computers (i.e. functions)
- functional limitations of AI
 - significance of problem definition
 - significance of context and human experience
 - &quot;framing&quot; the output
- moral limitations of AI


---
class: left, middle, main-title

# Readings

- [Greedy, Brittle, Opaque, Shallow (Wired)](https://www.wired.com/story/greedy-brittle-opaque-and-shallow-the-downsides-to-deep-learning/)
- [Norman the Psycopathic AI](http://norman-ai.mit.edu/)
- (previously, your class watched: [The Robots are Now Hiring (WSJ)](https://www.wsj.com/articles/artificial-intelligence-the-robots-are-now-hiring-moving-upstream-1537435820))


---
class: left, middle, main-title

# Hype around AI
Proponents (e.g. CEO in the HireVue interview) say AI is:


- &quot;scientific&quot;
- &quot;neutral&quot; / unbiased / objective
- &quot;intelligent&quot; (artificially, of course)

We&#39;re going to challenge each of those claims.


---
class: left, middle, main-title

# How does AI work?
But, first we need a quick primer on what computers even do.

Computers carry out *functions* for us.
A (mathematical) function defines a single output for any given input (or set of inputs).


---
class: left, middle, main-title

# Examples of functions (formulas)
Like in your algebra class:


- If we pass the *inputs*: `2` and `3`
- into the *function* `+`
- it should *output* `5`

You may think of these sorts of functions as formulas (like E=MC^2).


---
class: left, middle, main-title

# But, outside mathematics ...
|  function |  input |  output | 
|--|--|--|
|    SSN lookup |  a social security number |  a human (identified by names, address, etc) | 
|    HireVue service |  a video file |  an &quot;employability&quot; score (between 0% and 100%) | 
|    Norman |  an image file |  a short English sentence fragment description | 
|    &quot;Standard&quot; AI |  an image file |  a (*different*) English sentence fragment description | 

The key concern with calling something a function is that it returns the same output if you give it the same input again (will caveat this slightly later).


---
class: left, middle, main-title

# What do you see?
![black and white rorschach inkblot #6 (looks vaguely like a plane)](http://norman-ai.mit.edu/img/inkblots/Rorschach_blot_06.jpg)

???
Norman &quot;saw&quot;

> Man is shot dumped from car.

While the &quot;Standard&quot; AI &quot;saw&quot;:

> An airplane flying through the air with smoke coming from it.


---
class: left, middle, main-title

# What do you see (in color)?
<img
  src="http://norman-ai.mit.edu/img/inkblots/Rorschach_blot_10.jpg"
  alt="bright, primary colors rorschach inkblot #10 (complex, looks to me like two people kissing in pink sleeping bags"
  width="100%"
/>

???
Norman &quot;saw&quot;

> man killed by speeding driver.

While the &quot;Standard&quot; AI &quot;saw&quot;:

> a close up of a wedding cake on a table.


---
class: left, middle, main-title

# So what's an algorithm?
Many functions, we know as formulas or equations.
An algorithm is just a function (think algebra) defined via a recipe (think dinner).


---
class: left, middle, main-title

# But if I don't always get the same output from the same recipe!
Yes.

That&#39;s true, but nothing&#39;s ever exactly the same when *you* follow a recipe (different inputs).

With computer hardware, we try to make *everything* *exactly* the same every time.


---
class: left, middle, main-title

# Caveat
> when functions aren&#39;t?

We can also include &quot;stochastic&quot; or &quot;random&quot; functions, which allow different outputs.
For example, if you use the follow algorithm or recipe:


1. roll a dice (between 1 and 6)
1. set your oven to 100 degrees (Fahrenheit) times that (100 to 600 degrees)
1. place pizza dough in oven for that many minutes (1 - 6 minutes)

This counts as a function / algorithm / recipe, even if it isn&#39;t a very *good* (i.e. tasty) one.

The key to an algorithm or functions in general is that they are *predicatable* (well-defined).


---
class: left, middle, main-title

# Wait, does AI really follow simple recipes like this?
Yes.

Well, not always simple, but always rote and routine.


---
<img
  src="https://upload.wikimedia.org/wikipedia/commons/d/de/Complex_systems_organizational_map.jpg"
  alt="a digram categorizing complex evolving systems used in AI such as neural networks (wikipedia)"
  width="100%"
/>
![a digram categorizing complex evolving systems used in AI such as neural networks (wikipedia)](https://upload.wikimedia.org/wikipedia/commons/d/de/Complex_systems_organizational_map.jpg)

???
This image shows lots different kinds of &quot;AI&quot;.
As you can see, they are often based on complex natural systems (e.g. Ant Colony Optimization).

HireVue is probably using one or several of these approaches.
For example, what would HireVue do if you sent your favorite TikTok instead?
It&#39;s video, so it would give you an &quot;employability&quot; score.
And they use a function, so your friend would get the same score using the same video.

But would HireVue get the joke? Would it laugh? Would it be impressed? Would it hire you for social media?
No.


---
class: left, middle, main-title

# So how can we tell if an AI algorithm is good?
That&#39;s for the humans (you) to decide.

But let&#39;s think about some different ways to define what we mean by &quot;good&quot;.


---
class: left, middle, main-title

# Is AI intelligent?
Let&#39;s briefly make the case for what &quot;AI&quot; can do.

A major branch of AI, called Machine Learning (ML) is focused on helping machines &quot;learn&quot; things by looking at lots of data.

So, by feeding an imaging algorithm lots of pictures and labelling which are cats, it can &quot;learn&quot; to identify which other pictures also have cats in them.

The exciting thing about this is no one &quot;told&quot; it exactly how to identify a cat picture (e.g. check for eyes and ears ...).

And it can &quot;learn&quot; from picking up patterns in examples.


---
class: left, middle, main-title

# But ...
There is still a rote algorithm or recipe that tells it how to learn / what to look for.

It depends on the algorithm, but it may do really simple-*minded* things like, cut the picture into all kinds of different size rectangles and check whether each of those is similar to other cat photos it has &quot;seen&quot;.

In fact, so there still is a rigid component, designed by the humans to help it be successful with image processing.

It may also be good at identifying dogs, but it wouldn&#39;t be good at picking up sarcasm in humans sentences (a different algorithm).

So, a machine learning algorithm like this is intelligent in that it is really good at a skill and it learned &quot;on its own&quot;.

But, it&#39;s skill only applies in a very narrow context.


---
class: left, middle, main-title

# Is AI Objective?
In a sense, yes, but not the way that matters...


---
class: left, middle, main-title

# What does objective mean?

- subjective: a claim or perspective that depends on the speaker (&quot;it is cold&quot;)
- objective: a claim or perspective that depends only on the inert objects (&quot;it is 65 degrees Fahrenheit&quot;)
- inter-subjective: a claim or perspective shaped by a culture or community of perspectives (&quot;bread costs $6&quot;)


---
class: left, middle, main-title

# So an AI algorithm is objective
... but only in the sense that it is a function returning a single, predictable output given an input.

So, it&#39;s not making objective claims about the objects around it.
Rather, it is an object about which we can say what it will do and it will do the same thing every time.


---
class: left, middle, main-title

# But, that's not objectivity
When we talk about objectivity, we usually, mean unbiased scientific facts or at least a balanced perspective.

For example, in journalism, it means limited your claims to the things that aren&#39;t controversial, opinionated or subjective.

We can make objective predictions about what Norman will say, but &quot;his&quot; descriptions of photos are not &quot;objective&quot;.

The same is true for the &quot;Standard&quot; AI or HireVue.

The fact that those algorithms return the same result every time doesn&#39;t make it true.

It raises the question is hiring something any one (or thing) can even be objective about?


---
class: left, middle, main-title

# Is AI Morally Neutral?
Consider Norman the Psycopathic AI.

&quot;He&quot; certainly seems to put a downer on any image you show &quot;him&quot;.


---
class: left, middle, main-title

# Is Tech neutral?

- Some people claim guns are neutral.
- Others think something built to be a weapon is intrinsicaly harmful and bad.


---
class: left, middle, main-title

# Reconciling these views
Perhaps there&#39;s a spectrum?


- water bottle (mostly good uses, but can be turned violent)
- knife (50/50?)
- gun (intended to harm, but some good use cases)
- nuclear bomb (mostly bad ...)

???


---
class: left, middle, main-title

# Moral Malleability

- It still matters how you use something.
- But, somethings are harder to use for good (or for bad) (Malleability).
- As you get more specific, things get less ambiguous / neutral.

> This is just a proposal. Do you agree?

???
For example, more specific than &quot;knife&quot;: a paring knife vs. a katana


---
class: left, middle, main-title

# Forgive me

- I haven&#39;t presented on this before.
- I was really hoping to create a more intereactive experience (pre-virus).
- But, I am very open to feedback! :)


---
class: left, middle, main-title

# (Optional) Homework

- Later, we&#39;ll discuss human-first alternatives to Silicon Valley tech culture.
- [Pleroma](https://pleroma.social/) is an open source, community-built, federated alternative to twitter / facebook.
- It has no single top down filtering like main-stream, for-profit alternatives do.

Feel free to register in my [Pleroma](https://pleroma.social/) instance: https://pleroma.combinatorist.com/registration

???
Be warned, this is my first time hosting something like this.
The server might go down.
You might lose your data.
I will also moderate things you post on this pleroma instance if I or professor L feel its necessary.


		</textarea>
		<script src="https://gnab.github.io/remark/downloads/remark-latest.min.js"></script>
		<script>
			var slideshow = remark.create();
		</script>
		<script></script>
	</body>
</html>

