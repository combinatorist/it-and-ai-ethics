# How AI works (or doesn't)
This presentation talks about functional and ethical limitations of AI.

# Contributing
In case you want to tweak the presentation, edit the markdown file.
Then run:

    markdown-to-slides -d how-ai-works-or-doesnt-2020-03-21.md \
    | sed 's/class: center/class: left/' \
    > how-ai-works-or-doesnt-2020-03-21.md.sed.html

For some reason, I had to install markdown-to-slides globally for it to work. :(
